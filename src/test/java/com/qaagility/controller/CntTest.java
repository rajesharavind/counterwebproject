package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*; 


public class CntTest {


		@Test
		public void testCnt() throws Exception  {
		 
		      int result = new Counter().getCount(1,0);
		      assertEquals("Max value", Integer.MAX_VALUE, result);
		      
		      result = new Counter().getCount(1,1);
		      assertEquals("Divide by 1", 1, result);
		}
	
}