
package com.qaagility.controller;

public class Counter {

    public int getCount(int intvar1, int intvar2) {
        if (intvar2 == 0) 
        {
            return Integer.MAX_VALUE;
        }
        else
        {
            return intvar1 / intvar2;
        }
    }

}

